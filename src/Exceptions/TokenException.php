<?php
namespace MiniBell\Exceptions;

class TokenException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'TokenException';
    }

}