<?php
namespace MiniBell\Exceptions;

class ReserveException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'ReserveException';
    }

}