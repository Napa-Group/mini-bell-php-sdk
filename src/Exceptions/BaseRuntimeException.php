<?php
namespace MiniBell\Exceptions;

class BaseRuntimeException extends \RuntimeException 
{
    /**
     * BaseRuntimeException constructor.
     * @param $message
     * @param int $code
     */
    public function __construct($message, $code=0) {
        parent::__construct($message, $code);
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'BaseRuntimeException';
    }

    /**
     * @return string
     */
    public function errorMessage(){
        return "\r\n".$this->getName() . "[{$this->code}] : {$this->message}\r\n";
    }
}
