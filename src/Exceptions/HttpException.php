<?php 
namespace MiniBell\Exceptions;

class HttpException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'HttpException';
    }	
}
?>