<?php

namespace MiniBell\Exceptions;

use MiniBell\Entities\Error;

class ValidationException extends BaseRuntimeException
{
    private $errors = [];

    /**
     * ValidationException constructor.
     * @param Error[] $errors
     */
    public function __construct($errors)
    {
        $this->errors = $errors;
        parent::__construct($this->parseErrors($errors));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ValidationException';
    }

    /**
     * @return Error[] $errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return string
     */
    private function parseErrors($errors)
    {
        $msg = '';

        foreach ($errors as $error) {
            $msg .= $error->getMessage() . "\n";
        }

        return $msg;
    }
}