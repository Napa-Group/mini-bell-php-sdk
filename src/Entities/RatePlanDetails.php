<?php

namespace MiniBell\Entities;

class RatePlanDetails
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var int */
    private $breakfastRate;

    /** @var int */
    private $halfBoardRate;

    /** @var int */
    private $fullBoardRate;

    /** @var string */
    private $foodBoardType;

    /** @var boolean */
    private $cancelable;

    /** @var int */
    private $priority;

    /** @var int */
    private $sleeps;

    /** @var int */
    private $minStay;

    /** @var int */
    private $maxStay;

    /** @var boolean */
    private $disabled;

    /**
     * @var Facility[]
     */
    private $facilities;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getBreakfastRate()
    {
        return $this->breakfastRate;
    }

    /**
     * @param int $breakfastRate
     */
    public function setBreakfastRate($breakfastRate)
    {
        $this->breakfastRate = $breakfastRate;
    }

    /**
     * @return int
     */
    public function getHalfBoardRate()
    {
        return $this->halfBoardRate;
    }

    /**
     * @param int $halfBoardRate
     */
    public function setHalfBoardRate($halfBoardRate)
    {
        $this->halfBoardRate = $halfBoardRate;
    }

    /**
     * @return int
     */
    public function getFullBoardRate()
    {
        return $this->fullBoardRate;
    }

    /**
     * @param int $fullBoardRate
     */
    public function setFullBoardRate($fullBoardRate)
    {
        $this->fullBoardRate = $fullBoardRate;
    }

    /**
     * @return string
     */
    public function getFoodBoardType()
    {
        return $this->foodBoardType;
    }

    /**
     * @param string $foodBoardType
     */
    public function setFoodBoardType($foodBoardType)
    {
        $this->foodBoardType = $foodBoardType;
    }

    /**
     * @return bool
     */
    public function isCancelable()
    {
        return $this->cancelable;
    }

    /**
     * @param bool $cancelable
     */
    public function setCancelable($cancelable)
    {
        $this->cancelable = $cancelable;
    }

    /**
     * @return int
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * @param int $sleeps
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
        return $this->minStay;
    }

    /**
     * @param int $minStay
     */
    public function setMinStay($minStay)
    {
        $this->minStay = $minStay;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
        return $this->maxStay;
    }

    /**
     * @param int $maxStay
     */
    public function setMaxStay($maxStay)
    {
        $this->maxStay = $maxStay;
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @return Facility[]
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * @param Facility[] $facilities
     */
    public function setFacilities($facilities)
    {
        $this->facilities = $facilities;
    }
}