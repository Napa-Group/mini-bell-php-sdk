<?php

namespace MiniBell\Entities;

use MiniBell\Enums\ReserveVehicle;

class TransferReserveService
{
    /**
     * @var ReserveVehicle
     */
    private $vehicle;

    /**
     * @var array
     */
    private $vehicleNumber;

    /**
     * @var String
     */
    private $departureTime;

    /**
     * @var int
     */
    private $arrivalTime;

    /**
     * @var String
     */
    private $passengerPhone;

    /**
     * @var int
     */
    private $description;

    /**
     * @return ReserveVehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param ReserveVehicle $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return string
     */
    public function getVehicleNumber()
    {
        return $this->vehicleNumber;
    }

    /**
     * @param string $vehicleNumber
     */
    public function setVehicleNumber($vehicleNumber)
    {
        $this->vehicleNumber = $vehicleNumber;
    }

    /**
     * @return String
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    /**
     * @param String $departureTime
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;
    }

    /**
     * @return int
     */
    public function getArrivalTime()
    {
        return $this->arrivalTime;
    }

    /**
     * @param int $arrivalTime
     */
    public function setArrivalTime($arrivalTime)
    {
        $this->arrivalTime = $arrivalTime;
    }

    /**
     * @return String
     */
    public function getPassengerPhone()
    {
        return $this->passengerPhone;
    }

    /**
     * @param String $passengerPhone
     */
    public function setPassengerPhone($passengerPhone)
    {
        $this->passengerPhone = $passengerPhone;
    }

    /**
     * @return int
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'vehicle' => $this->getVehicle(),
            'vehicle_number' => $this->getVehicleNumber(),
            'departure_time' => $this->getDepartureTime(),
            'arrival_time' => $this->getArrivalTime(),
            'passenger_phone' => $this->getPassengerPhone(),
            'description' => $this->getDescription()
        ];
    }
}