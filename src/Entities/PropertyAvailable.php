<?php
namespace MiniBell\Entities;

class PropertyAvailable
{
    /**
     * @var int
     */
    private $propertyId;
    /**
     * @var RoomTypeAvailable[]
     */
    private $roomTypeAvailable;

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return RoomTypeAvailable[]
     */
    public function getRoomTypeAvailable()
    {
        return $this->roomTypeAvailable;
    }

    /**
     * @param RoomTypeAvailable[] $roomTypeAvailable
     */
    public function setRoomTypeAvailable($roomTypeAvailable)
    {
        $this->roomTypeAvailable = $roomTypeAvailable;
    }
}