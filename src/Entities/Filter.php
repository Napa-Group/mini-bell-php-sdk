<?php


namespace MiniBell\Entities;


class Filter
{
    private $columnName;
    private $operand;
    private $value;

    /**
     * @return mixed
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * @param mixed $columnName
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
    }

    /**
     * @return mixed
     */
    public function getOperand()
    {
        return $this->operand;
    }

    /**
     * @param mixed $operand
     */
    public function setOperand($operand): void
    {
        $this->operand = $operand;
    }


    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function toArray()
    {
        return [
            'column_name' => $this->columnName,
            'operand' => $this->operand,
            'value' => $this->value
        ];
    }
}