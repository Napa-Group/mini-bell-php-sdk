<?php

namespace MiniBell\Entities;

use MiniBell\Enums\PropertyServiceType;

class ReserveService
{
    /**
     * @var PropertyServiceType
     */
    private $type;

    /**
     * @var array
     */
    private $data;

    /**
     * @var String
     */
    private $description;

    /**
     * @return PropertyServiceType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param PropertyServiceType $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'type' => $this->getType(),
            'data' => $this->getData()->toArray(),
            'description' => $this->getDescription()
        ];
    }
}