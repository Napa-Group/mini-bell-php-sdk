<?php
namespace MiniBell\Entities;

class PropertyAvailableRoomRate
{
    /**
     * @var integer
     */
    private $propertyId;
    /**
     * @var RoomTypeAvailable[]
     */
   private $roomTypesAvailable;

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return RoomTypeAvailable[]
     */
    public function getRoomTypesAvailable()
    {
        return $this->roomTypesAvailable;
    }

    /**
     * @param RoomTypeAvailable[] $roomTypesAvailable
     */
    public function setRoomTypesAvailable($roomTypesAvailable)
    {
        $this->roomTypesAvailable = $roomTypesAvailable;
    }
}