<?php


namespace MiniBell\Entities;


class ReceiptResponse
{
    /** @var int */
    private $id;
    /** @var int */
    private $propertyId;
    /** @var int */
    private $agencyContractId;
    /** @var int */
    private $paymentChannelId;
    /** @var int */
    private $confirmationCode;
    /** @var string */
    private $paymentMethod;
    /** @var int */
    private $amount;
    /** @var int */
    private $referenceNumber;
    /** @var int */
    private $status;
    /** @var Data */
    private $data;
    /** @var int */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return int
     */
    public function getAgencyContractId()
    {
        return $this->agencyContractId;
    }

    /**
     * @param int $agencyContractId
     */
    public function setAgencyContractId($agencyContractId)
    {
        $this->agencyContractId = $agencyContractId;
    }

    /**
     * @return int
     */
    public function getPaymentChannelId()
    {
        return $this->paymentChannelId;
    }

    /**
     * @param int $paymentChannelId
     */
    public function setPaymentChannelId($paymentChannelId)
    {
        $this->paymentChannelId = $paymentChannelId;
    }

    /**
     * @return int
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * @param int $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param int $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return Data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param Data $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}