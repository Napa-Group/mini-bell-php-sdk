<?php

namespace MiniBell\Entities;

class ReserveRoomDetails
{
    /** @var int */
    private $roomTypeId;

    /** @var int */
    private $ratePlanId;

    /** @var int */
    private $count;

    /** @var int */
    private $adultCount;

    /** @var string */
    private $guestFirstName;

    /** @var string */
    private $guestLastName;

    /** @var string */
    private $guestPhone;

    /** @var string */
    private $guestEmail;

    /** @var string */
    private $guestNationalCode;

    /** @var string */
    private $guestPassportNumber;

    /** @var int */
    private $guestCountryId;

    /** @var int */
    private $guestCityId;

    /** @var int[] */
    private $childrenAges;

    /** @var int */
    private $totalRackPrice;

    /** @var int */
    private $totalAgencyPrice;

    /** @var int */
    private $totalGuestPrice;
    /**
     * @var ReserveRoomGuest[] $guest
     */
    private $guests;

    /**
     * @var RoomRate[] $prices
     */
    private $prices;

    /**
     * @var RoomTypeDetails
     */
    private $roomType;

    /**
     * @var RatePlanDetails
     */
    private $ratePlan;

    /**
     * @return mixed
     */
    public function getRoomTypeId()
    {
        return $this->roomTypeId;
    }

    /**
     * @param mixed $roomTypeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @return mixed
     */
    public function getRatePlanId()
    {
        return $this->ratePlanId;
    }

    /**
     * @param mixed $ratePlanId
     */
    public function setRatePlanId($ratePlanId)
    {
        $this->ratePlanId = $ratePlanId;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getAdultCount()
    {
        return $this->adultCount;
    }

    /**
     * @param mixed $adultCount
     */
    public function setAdultCount($adultCount)
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @return mixed
     */
    public function getGuestFirstName()
    {
        return $this->guestFirstName;
    }

    /**
     * @param mixed $guestFirstName
     */
    public function setGuestFirstName($guestFirstName)
    {
        $this->guestFirstName = $guestFirstName;
    }

    /**
     * @return mixed
     */
    public function getGuestLastName()
    {
        return $this->guestLastName;
    }

    /**
     * @param mixed $guestLastName
     */
    public function setGuestLastName($guestLastName)
    {
        $this->guestLastName = $guestLastName;
    }

    /**
     * @return mixed
     */
    public function getGuestPhone()
    {
        return $this->guestPhone;
    }

    /**
     * @param mixed $guestPhone
     */
    public function setGuestPhone($guestPhone)
    {
        $this->guestPhone = $guestPhone;
    }

    /**
     * @return mixed
     */
    public function getGuestEmail()
    {
        return $this->guestEmail;
    }

    /**
     * @param mixed $guestEmail
     */
    public function setGuestEmail($guestEmail)
    {
        $this->guestEmail = $guestEmail;
    }

    /**
     * @return mixed
     */
    public function getGuestNationalCode()
    {
        return $this->guestNationalCode;
    }

    /**
     * @param mixed $guestNationalCode
     */
    public function setGuestNationalCode($guestNationalCode)
    {
        $this->guestNationalCode = $guestNationalCode;
    }

    /**
     * @return mixed
     */
    public function getGuestPassportNumber()
    {
        return $this->guestPassportNumber;
    }

    /**
     * @param mixed $guestPassportNumber
     */
    public function setGuestPassportNumber($guestPassportNumber)
    {
        $this->guestPassportNumber = $guestPassportNumber;
    }

    /**
     * @return mixed
     */
    public function getGuestCountryId()
    {
        return $this->guestCountryId;
    }

    /**
     * @param mixed $guestCountryId
     */
    public function setGuestCountryId($guestCountryId)
    {
        $this->guestCountryId = $guestCountryId;
    }

    /**
     * @return mixed
     */
    public function getGuestCityId()
    {
        return $this->guestCityId;
    }

    /**
     * @param mixed $guestCityId
     */
    public function setGuestCityId($guestCityId)
    {
        $this->guestCityId = $guestCityId;
    }

    /**
     * @return mixed
     */
    public function getChildrenAges()
    {
        return $this->childrenAges;
    }

    /**
     * @param mixed $childrenAges
     */
    public function setChildrenAges($childrenAges)
    {
        $this->childrenAges = $childrenAges;
    }

    /**
     * @return mixed
     */
    public function getTotalRackPrice()
    {
        return $this->totalRackPrice;
    }

    /**
     * @param mixed $totalRackPrice
     */
    public function setTotalRackPrice($totalRackPrice)
    {
        $this->totalRackPrice = $totalRackPrice;
    }

    /**
     * @return int
     */
    public function getTotalAgencyPrice()
    {
        return $this->totalAgencyPrice;
    }

    /**
     * @param int $totalAgencyPrice
     */
    public function setTotalAgencyPrice($totalAgencyPrice)
    {
        $this->totalAgencyPrice = $totalAgencyPrice;
    }

    /**
     * @return int
     */
    public function getTotalGuestPrice()
    {
        return $this->totalGuestPrice;
    }

    /**
     * @param int $totalGuestPrice
     */
    public function setTotalGuestPrice($totalGuestPrice)
    {
        $this->totalGuestPrice = $totalGuestPrice;
    }

    /**
     * @return ReserveRoomGuest[]
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param ReserveRoomGuest[] $guests
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;
    }

    /**
     * @return RoomRate[]
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param RoomRate[] $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * @return RoomTypeDetails
     */
    public function getRoomType()
    {
        return $this->roomType;
    }

    /**
     * @param RoomTypeDetails $roomType
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;
    }

    /**
     * @return RatePlanDetails
     */
    public function getRatePlan()
    {
        return $this->ratePlan;
    }

    /**
     * @param RatePlanDetails $ratePlan
     */
    public function setRatePlan($ratePlan)
    {
        $this->ratePlan = $ratePlan;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        $properties = get_object_vars($this);

        $properties['guests'] = isset($properties['guests']) && is_array($properties['guests']) ? $properties['guests'] : [];
        foreach ($this->getGuests() as $guest) {
            $properties['guests'][] = $guest->toArray();
        }

        $properties['prices'] = [];
        foreach ($this->getPrices() as $rate) {
            $properties['prices'][] = $rate->toArray();
        }
        return $properties;
    }
}