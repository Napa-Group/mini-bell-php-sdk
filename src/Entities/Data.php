<?php


namespace MiniBell\Entities;


use MiniBell\Enums\ReceiptPaymentMethod;

class Data
{
    /** @var int */
    private $id;
    /** @var int */
    private $referenceNumber;
    /** @var int */
    private $uniqueNumber;
    /** @var int */
    private $paymentBillNumber;
    /** @var boolean */
    private $paid;
    /** @var boolean */
    private $canceled;
    /** @var int */
    private $cancelDate;
    /** @var boolean */
    private $verificationNeeded;
    /** @var string */
    private $result;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param int $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return int
     */
    public function getUniqueNumber()
    {
        return $this->uniqueNumber;
    }

    /**
     * @param int $uniqueNumber
     */
    public function setUniqueNumber($uniqueNumber)
    {
        $this->uniqueNumber = $uniqueNumber;
    }

    /**
     * @return int
     */
    public function getPaymentBillNumber()
    {
        return $this->paymentBillNumber;
    }

    /**
     * @param int $paymentBillNumber
     */
    public function setPaymentBillNumber($paymentBillNumber)
    {
        $this->paymentBillNumber = $paymentBillNumber;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        return $this->canceled;
    }

    /**
     * @param bool $canceled
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;
    }

    /**
     * @return int
     */
    public function getCancelDate()
    {
        return $this->cancelDate;
    }

    /**
     * @param int $cancelDate
     */
    public function setCancelDate($cancelDate)
    {
        $this->cancelDate = $cancelDate;
    }

    /**
     * @return bool
     */
    public function isVerificationNeeded()
    {
        return $this->verificationNeeded;
    }

    /**
     * @param bool $verificationNeeded
     */
    public function setVerificationNeeded($verificationNeeded)
    {
        $this->verificationNeeded = $verificationNeeded;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}