<?php

namespace MiniBell\Entities;

class RatePlanAvailable
{
    /**
     * @var int
     */
    private $ratePlanId;
    /**
     * @var RoomRate[]
     */
    private $roomRates;

    /**
     * @return int
     */
    public function getRatePlanId()
    {
        return $this->ratePlanId;
    }

    /**
     * @param int $ratePlanId
     */
    public function setRatePlanId($ratePlanId)
    {
        $this->ratePlanId = $ratePlanId;
    }

    /**
     * @return RoomRate[]
     */
    public function getRoomRates()
    {
        return $this->roomRates;
    }

    /**
     * @param RoomRate[] $roomRates
     */
    public function setRoomRates($roomRates)
    {
        $this->roomRates = $roomRates;
    }
}