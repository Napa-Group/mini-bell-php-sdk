<?php
namespace MiniBell\Entities;

class RoomTypeAvailable
{
    /**
     * @var int
     */
    private $roomTypeId;
    /**
     * @var RatePlanAvailable[]
     */
    private $ratePlansAvailable;

    /**
     * @return int
     */
    public function getRoomTypeId()
    {
        return $this->roomTypeId;
    }

    /**
     * @param int $roomTypeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @return RatePlanAvailable[]
     */
    public function getRatePlansAvailable()
    {
        return $this->ratePlansAvailable;
    }

    /**
     * @param RatePlanAvailable[] $ratePlansAvailable
     */
    public function setRatePlansAvailable($ratePlansAvailable)
    {
        $this->ratePlansAvailable = $ratePlansAvailable;
    }
}