<?php


namespace MiniBell\Entities;


use MiniBell\Enums\ReceiptPaymentMethod;

class Receipt
{
    /** @var int */
    private $confirmationCode;
    /** @var int */
    private $propertyId;
    /** @var int */
    private $amount;
    /** @var int */
    private $paymentChannelId;
    /** @var ReceiptPaymentMethod */
    private $paymentMethod;
    /** @var ReceiptDate */
    private $data;
    /** @var string */
    private $description;

    /**
     * @return int
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * @param int $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getPaymentChannelId()
    {
        return $this->paymentChannelId;
    }

    /**
     * @param int $paymentChannelId
     */
    public function setPaymentChannelId($paymentChannelId)
    {
        $this->paymentChannelId = $paymentChannelId;
    }

    /**
     * @return ReceiptPaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param ReceiptPaymentMethod $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return ReceiptDate
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param ReceiptData $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    public function toArray(){
        return [
            'confirmation_code' => $this->getConfirmationCode(),
            'property_id' => $this->getPropertyId(),
            'amount' => $this->getAmount(),
            'payment_channel_id' => $this->getPaymentChannelId(),
            'payment_method' => $this->getPaymentMethod(),
            'data' => $this->data->toArray(),
            'description' => $this->getDescription(),
        ];
    }
}