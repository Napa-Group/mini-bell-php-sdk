<?php

namespace MiniBell\Entities;

class RoomTypeDetails
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $type;

    /** @var int */
    private $capacity;

    /** @var int */
    private $extraCapacity;

    /** @var int */
    private $singleBedCount;

    /** @var int */
    private $doubleBedCount;

    /** @var int */
    private $sofaBedCount;

    /** @var int */
    private $count;

    /** @var boolean */
    private $outOfService;

    /** @var string */
    private $description;

    /**
     * @var RatePlanDetails[]
     */
    private $ratePlans;

    /**
     * @var Facility[]
     */
    private $facilities;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getExtraCapacity()
    {
        return $this->extraCapacity;
    }

    /**
     * @param int $extraCapacity
     */
    public function setExtraCapacity($extraCapacity)
    {
        $this->extraCapacity = $extraCapacity;
    }

    /**
     * @return int
     */
    public function getSingleBedCount()
    {
        return $this->singleBedCount;
    }

    /**
     * @param int $singleBedCount
     */
    public function setSingleBedCount($singleBedCount)
    {
        $this->singleBedCount = $singleBedCount;
    }

    /**
     * @return int
     */
    public function getDoubleBedCount()
    {
        return $this->doubleBedCount;
    }

    /**
     * @param int $doubleBedCount
     */
    public function setDoubleBedCount($doubleBedCount)
    {
        $this->doubleBedCount = $doubleBedCount;
    }

    /**
     * @return int
     */
    public function getSofaBedCount()
    {
        return $this->sofaBedCount;
    }

    /**
     * @param int $sofaBedCount
     */
    public function setSofaBedCount($sofaBedCount)
    {
        $this->sofaBedCount = $sofaBedCount;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return bool
     */
    public function isOutOfService()
    {
        return $this->outOfService;
    }

    /**
     * @param bool $outOfService
     */
    public function setOutOfService($outOfService)
    {
        $this->outOfService = $outOfService;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return RatePlanDetails[]
     */
    public function getRatePlans()
    {
        return $this->ratePlans;
    }

    /**
     * @param RatePlanDetails[] $ratePlans
     */
    public function setRatePlans($ratePlans)
    {
        $this->ratePlans = $ratePlans;
    }

    /**
     * @return Facility[]
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * @param Facility[] $facilities
     */
    public function setFacilities($facilities)
    {
        $this->facilities = $facilities;
    }
}