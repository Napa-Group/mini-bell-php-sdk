<?php
namespace MiniBell\Entities;

class ReserveRoom
{
    private $roomTypeId;
    private $ratePlanId;
    private $count;
    private $adultCount;
    private $childrenAges;
    private $guestFirstName;
    private $guestLastName;
    private $guestPhone;
    private $guestEmail;
    private $guestNationalCode;
    private $guestPassportNumber;
    private $guestCountryId;
    private $guestCityId;
    private $guests;

    /**
     * @param int $roomTypeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @param int $ratePlanId
     */
    public function setRatePlanId($ratePlanId)
    {
        $this->ratePlanId = $ratePlanId;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @param int $adultCount
     */
    public function setAdultCount($adultCount)
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @param int[] $childrenAges
     */
    public function setChildrenAges($childrenAges)
    {
        $this->childrenAges = $childrenAges;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string null $phone
     * @param string null $email
     * @param string null $nationalCode
     * @param string null $passportNumber
     * @param int null $countryId
     * @param int null $cityId
     */
    public function setGuest($firstName, $lastName, $phone = null, $email = null, $nationalCode = null, $passportNumber = null, $countryId = null, $cityId  = null)
    {
        $this->guestFirstName = $firstName;
        $this->guestLastName = $lastName;
        $this->guestPhone = $phone;
        $this->guestEmail = $email;
        $this->guestNationalCode = $nationalCode;
        $this->guestPassportNumber = $passportNumber;
        $this->guestCountryId = $countryId;
        $this->guestCityId = $cityId;
    }


    /**
     * @param $firstName
     * @param $lastName
     * @param null $phone
     * @param null $email
     * @param null $nationalCode
     * @param null $passportNumber
     * @param null $countryId
     * @param null $cityId
     */
    public function addGuests($firstName, $lastName, $phone = null, $email = null, $nationalCode = null, $passportNumber = null, $countryId = null, $cityId  = null)
    {
        $guest = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'phone' => $phone,
            'email' => $email,
            'national_code' => $nationalCode,
            'passport_number' => $passportNumber,
            'country_id' => $countryId,
            'city_id' => $cityId];
        $this->guests[] = $guest;
    }

    /**
     * @return array
     */
    public function toArray(){
        return [
            'room_type_id' => $this->roomTypeId,
            'rate_plan_id' => $this->ratePlanId,
            'count' => $this->count,
            'adult_count' => $this->adultCount,
            'children_ages' => $this->childrenAges,
            'guest_first_name' => $this->guestFirstName,
            'guest_last_name' => $this->guestLastName,
            'guest_phone' => $this->guestPhone,
            'guest_email' => $this->guestEmail,
            'guest_national_code' => $this->guestNationalCode,
            'guest_passport_number' => $this->guestPassportNumber,
            'guest_country_id' => $this->guestCountryId,
            'guest_city_id' => $this->guestCityId,
            'guests' => isset($this->guests) && is_array($this->guests) ? $this->guests : []
        ];
    }

}