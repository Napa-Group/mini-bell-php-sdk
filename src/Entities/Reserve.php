<?php
namespace MiniBell\Entities;

class Reserve
{

    private $propertyId;
    private $checkIn;
    private $checkOut;
    private $bookerFirstName;
    private $bookerLastName;
    private $bookerPhone;
    private $bookerEmail;
    private $vehicle;
    private $vehicleNumber;
    private $description;
    private $agent;
    private $rooms = [];

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @param string $checkIn
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;
    }

    /**
     * @param string $checkOut
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string null $phone
     * @param string null $email
     */
    public function setBooker($firstName, $lastName, $phone = null, $email = null )
    {
        $this->bookerFirstName = $firstName;
        $this->bookerLastName = $lastName;
        $this->bookerPhone = $phone;
        $this->bookerEmail = $email;
    }
    /**
     * @param string $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @param string $vehicleNumber
     */
    public function setVehicleNumber($vehicleNumber)
    {
        $this->vehicleNumber = $vehicleNumber;
    }

    /**
     * @param ReserveRoom $room
     */
    public function addRoom(ReserveRoom $room )
    {
        $this->rooms[] = $room;
    }

    /**
     * @return array
     */
    public function toArray(){
        $rooms = [];
        foreach ( $this->rooms as $room ){
            $rooms[] = $room->toArray();
        }

        return [
            'property_id' => $this->propertyId,
            'check_in' => $this->checkIn,
            'check_out' => $this->checkOut,
            'booker_first_name' => $this->bookerFirstName,
            'booker_last_name' => $this->bookerLastName,
            'booker_phone' => $this->bookerPhone,
            'booker_email' => $this->bookerEmail,
            'vehicle' => $this->vehicle,
            'vehicle_number' => $this->vehicleNumber,
            'rooms' => $rooms,
            'description' => $this->description,
            'agent' => $this->agent
        ];
    }
}