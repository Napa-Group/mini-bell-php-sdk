<?php
namespace MiniBell\Entities;

class ReserveResponse
{
    /** @var ReserveDetails */
    private $reserveDetails;

    /** @var Error[] */
    private $errors = [];

    /**
     * @return ReserveDetails
     */
    public function getReserveDetails()
    {
        return $this->reserveDetails;
    }

    /**
     * @param ReserveDetails $reserve
     */
    public function setReserveDetails($reserveDetails)
    {
        $this->reserveDetails = $reserveDetails;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param Error[] $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param Error $error
     */
    public function addError($error)
    {
        $this->errors[] = $error;
    }

}