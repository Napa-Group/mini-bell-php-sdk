<?php

namespace MiniBell\Entities;

class Facility
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var int */
    private $facilityGroupId;
    /** @var string */
    private $facilityGroupName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getFacilityGroupId()
    {
        return $this->facilityGroupId;
    }

    /**
     * @param int $facilityGroupId
     */
    public function setFacilityGroupId($facilityGroupId)
    {
        $this->facilityGroupId = $facilityGroupId;
    }

    /**
     * @return string
     */
    public function getFacilityGroupName()
    {
        return $this->facilityGroupName;
    }

    /**
     * @param string $facilityGroupName
     */
    public function setFacilityGroupName($facilityGroupName)
    {
        $this->facilityGroupName = $facilityGroupName;
    }
}