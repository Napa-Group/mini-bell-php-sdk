<?php

namespace MiniBell\Entities;

use DateTime;

class RoomRate
{
    /** @var DateTime */
    private $day;

    /** @var int */
    private $inventory;

    /** @var int */
    private $rackRate;

    /** @var int */
    private $babyCotRackRate;

    /** @var int */
    private $extendBedRackRate;

    /** @var int */
    private $agencyRate;

    /** @var int */
    private $babyCotAgencyRate;

    /** @var int */
    private $extendBedAgencyRate;

    /** @var int */
    private $guestRate;

    /** @var int */
    private $babyCotGuestRate;

    /** @var int */
    private $extendBedGuestRate;

    /** @var int */
    private $minStay;

    /** @var int */
    private $maxStay;

    /** @var boolean */
    private $closeToArrival;

    /** @var boolean */
    private $closeToDeparture;

    /** @var boolean */
    private $closed;

    /**
     * @return DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param DateTime $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param int $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return int
     */
    public function getRackRate()
    {
        return $this->rackRate;
    }

    /**
     * @param int $rackRate
     */
    public function setRackRate($rackRate)
    {
        $this->rackRate = $rackRate;
    }

    /**
     * @return int
     */
    public function getBabyCotRackRate()
    {
        return $this->babyCotRackRate;
    }

    /**
     * @param int $babyCotRackRate
     */
    public function setBabyCotRackRate($babyCotRackRate)
    {
        $this->babyCotRackRate = $babyCotRackRate;
    }

    /**
     * @return int
     */
    public function getExtendBedRackRate()
    {
        return $this->extendBedRackRate;
    }

    /**
     * @param int $extendBedRackRate
     */
    public function setExtendBedRackRate($extendBedRackRate)
    {
        $this->extendBedRackRate = $extendBedRackRate;
    }

    /**
     * @return int
     */
    public function getAgencyRate()
    {
        return $this->agencyRate;
    }

    /**
     * @param int $agencyRate
     */
    public function setAgencyRate($agencyRate)
    {
        $this->agencyRate = $agencyRate;
    }

    /**
     * @return int
     */
    public function getBabyCotAgencyRate()
    {
        return $this->babyCotAgencyRate;
    }

    /**
     * @param int $babyCotAgencyRate
     */
    public function setBabyCotAgencyRate($babyCotAgencyRate)
    {
        $this->babyCotAgencyRate = $babyCotAgencyRate;
    }

    /**
     * @return int
     */
    public function getExtendBedAgencyRate()
    {
        return $this->extendBedAgencyRate;
    }

    /**
     * @param int $extendBedAgencyRate
     */
    public function setExtendBedAgencyRate($extendBedAgencyRate)
    {
        $this->extendBedAgencyRate = $extendBedAgencyRate;
    }

    /**
     * @return int
     */
    public function getGuestRate()
    {
        return $this->guestRate;
    }

    /**
     * @param int $guestRate
     */
    public function setGuestRate($guestRate)
    {
        $this->guestRate = $guestRate;
    }

    /**
     * @return int
     */
    public function getBabyCotGuestRate()
    {
        return $this->babyCotGuestRate;
    }

    /**
     * @param int $babyCotGuestRate
     */
    public function setBabyCotGuestRate($babyCotGuestRate)
    {
        $this->babyCotGuestRate = $babyCotGuestRate;
    }

    /**
     * @return int
     */
    public function getExtendBedGuestRate()
    {
        return $this->extendBedGuestRate;
    }

    /**
     * @param int $extendBedGuestRate
     */
    public function setExtendBedGuestRate($extendBedGuestRate)
    {
        $this->extendBedGuestRate = $extendBedGuestRate;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
        return $this->minStay;
    }

    /**
     * @param int $minStay
     */
    public function setMinStay($minStay)
    {
        $this->minStay = $minStay;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
        return $this->maxStay;
    }

    /**
     * @param int $maxStay
     */
    public function setMaxStay($maxStay)
    {
        $this->maxStay = $maxStay;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->closed;
    }

    /**
     * @return bool
     */
    public function isCloseToArrival()
    {
        return $this->closeToArrival;
    }

    /**
     * @param bool $closeToArrival
     */
    public function setCloseToArrival($closeToArrival)
    {
        $this->closeToArrival = $closeToArrival;
    }

    /**
     * @return bool
     */
    public function isCloseToDeparture()
    {
        return $this->closeToDeparture;
    }

    /**
     * @param bool $closeToDeparture
     */
    public function setCloseToDeparture($closeToDeparture)
    {
        $this->closeToDeparture = $closeToDeparture;
    }

    /**
     * @param bool $closed
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    }
}