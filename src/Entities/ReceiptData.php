<?php


namespace MiniBell\Entities;


use MiniBell\Enums\BankName;
use MiniBell\Enums\ReceiptPaymentMethod;

class ReceiptData
{
    /** @var string */
    private $redirectUrl;
    /** @var string */
    private $phoneNumber;
    /** @var string */
    private $originAccountOwner;
    /** @var BankName */
    private $originAccountBankName;
    /** @var string */
    private $originAccountNumber;
    /** @var string */
    private $originAccountCardNumber;
    /** @var string */
    private $originAccountIban;
    /** @var string */
    private $destinationAccountOwner;
    /** @var BankName */
    private $destinationAccountBankName;
    /** @var string */
    private $destinationAccountNumber;
    /** @var string */
    private $destinationAccountCardNumber;
    /** @var string */
    private $destinationAccountIban;
    /** @var string */
    private $paidDate;
    /** @var string */
    private $referenceNumber;

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getOriginAccountOwner()
    {
        return $this->originAccountOwner;
    }

    /**
     * @param string $originAccountOwner
     */
    public function setOriginAccountOwner($originAccountOwner)
    {
        $this->originAccountOwner = $originAccountOwner;
    }

    /**
     * @return BankName
     */
    public function getOriginAccountBankName()
    {
        return $this->originAccountBankName;
    }

    /**
     * @param BankName $originAccountBankName
     */
    public function setOriginAccountBankName($originAccountBankName)
    {
        $this->originAccountBankName = $originAccountBankName;
    }

    /**
     * @return string
     */
    public function getOriginAccountNumber()
    {
        return $this->originAccountNumber;
    }

    /**
     * @param string $originAccountNumber
     */
    public function setOriginAccountNumber($originAccountNumber)
    {
        $this->originAccountNumber = $originAccountNumber;
    }

    /**
     * @return string
     */
    public function getOriginAccountCardNumber()
    {
        return $this->originAccountCardNumber;
    }

    /**
     * @param string $originAccountCardNumber
     */
    public function setOriginAccountCardNumber($originAccountCardNumber)
    {
        $this->originAccountCardNumber = $originAccountCardNumber;
    }

    /**
     * @return string
     */
    public function getOriginAccountIban()
    {
        return $this->originAccountIban;
    }

    /**
     * @param string $originAccountIban
     */
    public function setOriginAccountIban($originAccountIban)
    {
        $this->originAccountIban = $originAccountIban;
    }

    /**
     * @return string
     */
    public function getDestinationAccountOwner()
    {
        return $this->destinationAccountOwner;
    }

    /**
     * @param string $destinationAccountOwner
     */
    public function setDestinationAccountOwner($destinationAccountOwner)
    {
        $this->destinationAccountOwner = $destinationAccountOwner;
    }

    /**
     * @return BankName
     */
    public function getDestinationAccountBankName()
    {
        return $this->destinationAccountBankName;
    }

    /**
     * @param BankName $destinationAccountBankName
     */
    public function setDestinationAccountBankName($destinationAccountBankName)
    {
        $this->destinationAccountBankName = $destinationAccountBankName;
    }

    /**
     * @return string
     */
    public function getDestinationAccountNumber()
    {
        return $this->destinationAccountNumber;
    }

    /**
     * @param string $destinationAccountNumber
     */
    public function setDestinationAccountNumber($destinationAccountNumber)
    {
        $this->destinationAccountNumber = $destinationAccountNumber;
    }

    /**
     * @return string
     */
    public function getDestinationAccountCardNumber()
    {
        return $this->destinationAccountCardNumber;
    }

    /**
     * @param string $destinationAccountCardNumber
     */
    public function setDestinationAccountCardNumber($destinationAccountCardNumber)
    {
        $this->destinationAccountCardNumber = $destinationAccountCardNumber;
    }

    /**
     * @return string
     */
    public function getDestinationAccountIban()
    {
        return $this->destinationAccountIban;
    }

    /**
     * @param string $destinationAccountIban
     */
    public function setDestinationAccountIban($destinationAccountIban)
    {
        $this->destinationAccountIban = $destinationAccountIban;
    }

    /**
     * @return string
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * @param string $paidDate
     */
    public function setPaidDate($paidDate)
    {
        $this->paidDate = $paidDate;
    }

    /**
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param string $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'redirect_url' => $this->getRedirectUrl(),
            'phone_number' => $this->getPhoneNumber(),
            'paid_date' => $this->getPaidDate(),
            'reference_number' => $this->getReferenceNumber(),
            'origin_account_owner' => $this->getOriginAccountOwner(),
            'origin_account_bank_name' => $this->getOriginAccountBankName(),
            'origin_account_number' => $this->getOriginAccountNumber(),
            'origin_account_card_number' => $this->getOriginAccountCardNumber(),
            'origin_account_iban' => $this->getOriginAccountIban(),
            'destination_account_owner' => $this->getDestinationAccountOwner(),
            'destination_account_bank_name' => $this->getDestinationAccountBankName(),
            'destination_account_number' => $this->getDestinationAccountNumber(),
            'destination_account_card_number' => $this->getDestinationAccountCardNumber(),
            'destination_account_iban' => $this->getDestinationAccountIban(),
        ];
    }
}