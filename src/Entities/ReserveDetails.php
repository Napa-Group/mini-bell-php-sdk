<?php

namespace MiniBell\Entities;

use DateTime;

class ReserveDetails
{
    /**
     * @var int
     */
    private $propertyId;
    /**
     * @var DateTime
     */
    private $checkIn;
    /**
     * @var DateTime
     */
    private $checkOut;
    /**
     * @var string
     */
    private $confirmationCode;
    /**
     * @var string
     */
    private $bookerFirstName;
    /**
     * @var string
     */
    private $bookerLastName;
    /**
     * @var string
     */
    private $bookerPhone;
    /**
     * @var string
     */
    private $bookerEmail;
    /**
     * @var string
     */
    private $vehicle;
    /**
     * @var string
     */
    private $vehicleNumber;
    /**
     * @var string
     */
    private $state;
    /**
     * @var string
     */
    private $status;
    /**
     * @var int
     */
    private $discount;
    /**
     * @var int
     */
    private $totalRackPrice;

    /**
     * @var int
     */
    private $totalAgencyPrice;

    /**
     * @var int
     */
    private $totalGuestPrice;
    /**
     * @var string
     */
    private $description;
    /**
     * @var DateTime
     */
    private $createDate;
    /**
     * @var DateTime
     */
    private $expireDate;
    /**
     * @var DateTime
     */
    private $cancelDate;
    /**
     * @var ReserveRoomDetails[] $rooms
     */
    private $rooms;

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return DateTime
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param DateTime $checkIn
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;
    }

    /**
     * @return DateTime
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param DateTime $checkOut
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;
    }

    /**
     * @return string
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * @param string $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * @return string
     */
    public function getBookerFirstName()
    {
        return $this->bookerFirstName;
    }

    /**
     * @param string $bookerFirstName
     */
    public function setBookerFirstName($bookerFirstName)
    {
        $this->bookerFirstName = $bookerFirstName;
    }

    /**
     * @return string
     */
    public function getBookerLastName()
    {
        return $this->bookerLastName;
    }

    /**
     * @param string $bookerLastName
     */
    public function setBookerLastName($bookerLastName)
    {
        $this->bookerLastName = $bookerLastName;
    }

    /**
     * @return string
     */
    public function getBookerPhone()
    {
        return $this->bookerPhone;
    }

    /**
     * @param string $bookerPhone
     */
    public function setBookerPhone($bookerPhone)
    {
        $this->bookerPhone = $bookerPhone;
    }

    /**
     * @return string
     */
    public function getBookerEmail()
    {
        return $this->bookerEmail;
    }

    /**
     * @param string $bookerEmail
     */
    public function setBookerEmail($bookerEmail)
    {
        $this->bookerEmail = $bookerEmail;
    }

    /**
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param string $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return string
     */
    public function getVehicleNumber()
    {
        return $this->vehicleNumber;
    }

    /**
     * @param string $vehicleNumber
     */
    public function setVehicleNumber($vehicleNumber)
    {
        $this->vehicleNumber = $vehicleNumber;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return int
     */
    public function getTotalRackPrice()
    {
        return $this->totalRackPrice;
    }

    /**
     * @param int $totalRackPrice
     */
    public function setTotalRackPrice($totalRackPrice)
    {
        $this->totalRackPrice = $totalRackPrice;
    }

    /**
     * @return int
     */
    public function getTotalAgencyPrice()
    {
        return $this->totalAgencyPrice;
    }

    /**
     * @param int $totalAgencyPrice
     */
    public function setTotalAgencyPrice($totalAgencyPrice)
    {
        $this->totalAgencyPrice = $totalAgencyPrice;
    }

    /**
     * @return int
     */
    public function getTotalGuestPrice()
    {
        return $this->totalGuestPrice;
    }

    /**
     * @param int $totalGuestPrice
     */
    public function setTotalGuestPrice($totalGuestPrice)
    {
        $this->totalGuestPrice = $totalGuestPrice;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param DateTime $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return DateTime
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @param DateTime $expireDate
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    }

    /**
     * @return DateTime
     */
    public function getCancelDate()
    {
        return $this->cancelDate;
    }

    /**
     * @param DateTime $cancelDate
     */
    public function setCancelDate($cancelDate)
    {
        $this->cancelDate = $cancelDate;
    }

    /**
     * @return ReserveRoomDetails[]
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param ReserveRoomDetails[] $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $properties = get_object_vars($this);
        $properties['rooms'] = [];
        foreach ($this->getRooms() as $reserveRoomDetails) {
            $properties['rooms'][] = $reserveRoomDetails->toArray();
        }
        return $properties;
    }
}