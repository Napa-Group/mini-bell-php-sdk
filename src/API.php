<?php

namespace MiniBell;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use MiniBell\Entities\Property;
use MiniBell\Entities\Receipt;
use MiniBell\Entities\ReserveResponse;
use MiniBell\Entities\ReserveService;
use MiniBell\Enums\HttpStatusCodes;
use MiniBell\Enums\PropertyServiceType;
use MiniBell\Entities\PropertyDetails;
use MiniBell\Entities\Reserve;
use MiniBell\Entities\ReserveDetails;
use MiniBell\Exceptions\BaseRuntimeException;
use MiniBell\Exceptions\HttpException;
use MiniBell\Exceptions\TokenException;
use MiniBell\Exceptions\ValidationException;
use MiniBell\Factories\ErrorFactory;
use MiniBell\Factories\PropertyAvailableRoomRatesFactory;
use MiniBell\Factories\PropertyDetailsFactory;
use MiniBell\Factories\PropertyFactory;
use MiniBell\Factories\PropertyPaymentChannelsFactory;
use MiniBell\Factories\ReceiptResponseFactory;
use MiniBell\Factories\ReserveDetailsFactory;
use MiniBell\Factories\ReserveResponseFactory;
use MiniBell\Factories\ReserveServiceResponseFactory;
use MiniBell\Factories\TransferReserveServiceFactory;

class API
{
    protected $token;

    protected $host;

    protected $client;

    private $request;

    private $response;

    const VERSION = 'v1';

    /**
     * API constructor.
     * @param string $host
     * @param string $token
     * @param $client
     */
    public function __construct($token, $host, $client = null)
    {
        $this->host = $host;
        $this->token = $token;
        $this->client = $client ? $client : new Client();
    }

    /**
     * @param string $methodName
     * @return string
     */
    protected function getPath($methodName)
    {
        return $this->host . '/' . self::VERSION . '/' . $methodName;
    }

    /**
     * @param $requestType
     * @param $methodName
     * @param null $data
     * @return \stdClass
     */
    private function _call($requestType, $methodName, $data = null)
    {
        try {
            $url = $this->getPath($methodName);
            $headers = [
                'Content-Type' => 'application/json',
                'Client-Token' => $this->token
            ];
            $body = null;
            if (!empty($data)) {
                if (strtolower($requestType) == 'get') {
                    $url .= '?' . http_build_query($data);
                } else {
                    $body = json_encode($data);
                }
            }
            $this->request = new Request($requestType, $url, $headers, $body);
            $this->response = $this->client->send($this->request);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            if ($this->response) {
                $result = (string)$this->response->getBody();
                $result = json_decode($result);

                switch ($this->response->getStatusCode()) {
                    case HttpStatusCodes::UNPROCESSABLE_ENTITY:
                        throw new ValidationException(ErrorFactory::makeCollection($result->errors));
                        break;
                }
            }
            throw new BaseRuntimeException($e->getMessage(), $e->getCode());
        }

        $result = json_decode($this->response->getBody());
        if (is_null($result)) {
            throw new HttpException("Response is empty!", HttpStatusCodes::INTERNAL_SERVER_ERROR);
        } else {

            return $result->value;
        }
    }

    /**
     * @param null $offset optional
     * @param null $count optional
     * @param null $count optional
     * @return Property[]
     */
    public function getProperties($offset = null, $count = null, &$total = null, $filters = null, $orders = null)
    {
        $result = $this->_call('GET', 'properties', [
            'offset' => $offset,
            'count' => $count,
            'filters' => $filters,
            'orders' => $orders
        ]);
        $total = $result->total;

        return PropertyFactory::makeCollection($result->properties);
    }

    /**
     * @param $id
     * @return PropertyDetails
     */
    public function getProperty($id)
    {
        $result = $this->_call('GET', 'properties/' . $id);

        return PropertyDetailsFactory::make($result->property);
    }

    /**
     * @param Reserve $reserve
     * @return ReserveResponse
     */
    public function reserve(Reserve $reserve)
    {
        try {
            $this->_call('POST', 'reserves/reserve', $reserve->toArray());
        } catch (ValidationException $e) {
            //
        }

        return ReserveResponseFactory::make(json_decode($this->response->getBody()));
    }

    /**
     * @param $confirmationCode
     * @return ReserveResponse
     */
    public function book($confirmationCode)
    {
        $result = $this->_call('POST', 'reserves/' . $confirmationCode . '/book');
        return ReserveResponseFactory::make(json_decode($this->response->getBody()));
    }

    /**
     * @param $confirmationCode
     * @return ReserveResponse
     */
    public function extendedExpired($confirmationCode)
    {
        $result = $this->_call('POST', 'reserves/' . $confirmationCode . '/extended-expired');
        return ReserveResponseFactory::make(json_decode($this->response->getBody()));
    }

    /**
     * @param $confirmationCode
     * @param ReserveService $reserveService
     * @return ReserveServiceResponse
     */
    public function requestReserveService($confirmationCode, $reserveService)
    {
        $result = $this->_call('POST', 'reserves/' . $confirmationCode . '/request-service', $reserveService->toArray());
        $reserveServiceResponse = ReserveServiceResponseFactory::make($result->reserve_service);

        $data = null;

        switch ($reserveServiceResponse->gettype()) {
            case PropertyServiceType::TRANSFER:
                $data = TransferReserveServiceFactory::make($result->reserve_service->data);
                break;
        }

        $reserveServiceResponse->setData($data);

        return $reserveServiceResponse;
    }

    /**
     * @return ReserveDetails[]
     */
    public function reserves($offset = null, $count = null, &$total = null, $filters = null, $orders = null)
    {
        $result = $this->_call('GET', 'reserves', [
            'offset' => $offset,
            'count' => $count,
            'filters' => $filters,
            'orders' => $orders
        ]);
        $total = $result->total;

        return ReserveDetailsFactory::makeCollection($result->reserves);
    }

    /**
     * @param $confirmationCode
     * @return ReserveResponse
     */
    public function getReserveByConfirmationCode($confirmationCode)
    {
        $result = $this->_call('GET', 'reserves/' . $confirmationCode);
        return ReserveResponseFactory::make(json_decode($this->response->getBody()));
    }

    /**
     * @return String
     */
    public function getWebHook()
    {
        $result = $this->_call('GET', 'web-hook');
        return $result->web_hook;
    }

    /**
     * @param $url
     * @return bool
     */
    public function setWebHook($url)
    {
        $this->_call('POST', 'web-hook', ['web_hook' => $url]);
        return true;
    }

    /**
     * @param Receipt $receipt
     * @return String
     */
    public function sendReceipt(Receipt $receipt)
    {
        $result = $this->_call('POST', 'receipts/request-payment/', $receipt->toArray());
        return ReceiptResponseFactory::make($result->receipt);
    }

    public function getPropertyPaymentChannels($propertyId, $filters)
    {
        $result = $this->_call('GET', 'properties/' . $propertyId . '/payment-channels', [
            'filters' => $filters,
        ]);
        return PropertyPaymentChannelsFactory::makeCollection($result->payment_channels);
    }

    public function getAvailableRoomRates($propertyId, $roomTypeId, $ratePlanId, $fromDate, $toDate)
    {
        $result = $this->_call('GET', 'properties/' . $propertyId . '/available-room-rates', [
            'room_type_id' => $roomTypeId,
            'rate_plan_id' => $ratePlanId,
            'from_date' => $fromDate->format('Y-m-d'),
            'to_date' => $toDate->format('Y-m-d'),
        ]);
        return PropertyAvailableRoomRatesFactory::makeCollection($result->room_rates);
    }


    public function requestService($confirmationCode, ReserveService $reserveService)
    {
        $result = $this->_call('POST', 'reserves/' . $confirmationCode . '/request-service', $reserveService->toArray());
        return ReserveServiceResponseFactory::make($result->reserve_service);
    }
}
