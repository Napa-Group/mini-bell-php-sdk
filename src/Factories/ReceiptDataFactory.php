<?php


namespace MiniBell\Factories;


use MiniBell\Entities\ReceiptData;

class ReceiptDataFactory
{
    public static function make($entity)
    {
        $receiptData = new ReceiptData();
        $receiptData->setRedirectUrl($entity->redirect_url);
        $receiptData->setPhoneNumber($entity->phone_number);
        $receiptData->setPaidDate($entity->paid_date);
        $receiptData->setReferenceNumber($entity->reference_Number);
        $receiptData->setDestinationAccountBankName($entity->destination_account_bank_name);
        $receiptData->setDestinationAccountCartNumber($entity->destination_account_card_number);
        $receiptData->setDestinationAccountIban($entity->destination_account_iban);
        $receiptData->setDestinationAccountNumber($entity->destination_account_number);
        $receiptData->setDestinationAccountOwner($entity->destination_account_owner);
        $receiptData->setOrginAccountBankName($entity->destination_account_bank_name);
        $receiptData->setOrginAccountCartNumber($entity->orgin_account_cart_number);
        $receiptData->setOrginAccountIban($entity->orgin_account_iban);
        $receiptData->setOrginAccountNumber($entity->orgin_account_number);
        $receiptData->setOrginAccountOwner($entity->orgin_account_owner);

        return $receiptData;
    }

    public static function makeCollection($entities)
    {
        $receiptDataCollection = [];
        foreach ($entities as $entity) {
            $receiptDataCollection[] = self::make($entity);
        }

        return $receiptDataCollection;
    }

}