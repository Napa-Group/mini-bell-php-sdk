<?php

namespace MiniBell\Factories;

use Exception;
use MiniBell\Entities\RoomTypeAvailable;

class RoomTypeAvailableFactory implements IFactory
{
    /**
     * @param $entity
     * @return RoomTypeAvailable
     * @throws Exception
     */
    public static function make( $entity ){
        $roomTypeAvailable = new RoomTypeAvailable();

        $roomTypeAvailable->setRoomTypeId($entity->room_type_id);
        $roomTypeAvailable->setRatePlansAvailable(RatePlanAvailableFactory::makeCollection($entity->rate_plans_available));

        return $roomTypeAvailable;
    }

    /**
     * @param $entities
     * @return RoomTypeAvailable[]
     * @throws Exception
     */
    public static function makeCollection($entities)
    {
        $roomTypesAvailable = [];
        foreach ( $entities as $entity ){
            $roomTypesAvailable[] = self::make($entity);
        }

        return $roomTypesAvailable;
    }

}