<?php

namespace MiniBell\Factories;

use MiniBell\Entities\Facility;

class FacilityFactory implements IFactory
{

    /**
     * @param $entity
     * @return Facility
     */
    public static function make($entity)
    {
        $facility = new Facility();

        $facility->setId($entity->id);
        $facility->setName($entity->name);
        $facility->setFacilityGroupId($entity->facility_group_id);
        $facility->setFacilityGroupName($entity->facility_group_name ?? null);

        return $facility;
    }

    /**
     * @param $entities
     * @return Facility[]
     */
    public static function makeCollection($entities)
    {
        $facilities = [];

        foreach ($entities as $entity) {
            $facilities[] =self::make($entity);
        }

        return $facilities;
    }
}