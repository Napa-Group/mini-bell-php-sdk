<?php

namespace MiniBell\Factories;

use MiniBell\Entities\RatePlanDetails;

class RatePlanDetailsFactory implements IFactory
{
    /**
     * @param $entity
     * @return RatePlanDetails
     */
    public static function make($entity)
    {
        $ratePlan = new RatePlanDetails();

        $ratePlan->setId($entity->id);
        $ratePlan->setName($entity->name);
        $ratePlan->setBreakfastRate($entity->breakfast_rate ?? null);
        $ratePlan->setHalfBoardRate($entity->half_board_rate ?? null);
        $ratePlan->setFullBoardRate($entity->full_board_rate ?? null);
        $ratePlan->setFoodBoardType($entity->food_board_rate ?? null);
        $ratePlan->setCancelable($entity->cancelable ?? null);
        $ratePlan->setPriority($entity->priority ?? null);
        $ratePlan->setSleeps($entity->sleeps);
        $ratePlan->setMinStay($entity->min_stay);
        $ratePlan->setMaxStay($entity->max_stay);
        $ratePlan->setDisabled($entity->disabled);
        $ratePlan->setFacilities(FacilityFactory::makeCollection($entity->facilities));

        return $ratePlan;
    }

    /**
     * @param $entities
     * @return RatePlanDetails[]
     */
    public static function makeCollection($entities)
    {
        $ratePlans = [];
        foreach ($entities as $entity) {
            $ratePlans[] = self::make($entity);
        }

        return $ratePlans;
    }
}