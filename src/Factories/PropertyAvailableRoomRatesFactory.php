<?php


namespace MiniBell\Factories;


use MiniBell\Entities\PropertyAvailableRoomRate;

class PropertyAvailableRoomRatesFactory implements IFactory
{
    /**
     * @param $entity
     * @return PropertyAvailableRoomRate
     */
    public static function make($entity)
    {
        $propertyAvailableRoomRate = new PropertyAvailableRoomRate();

        $propertyAvailableRoomRate->setPropertyId($entity->property_id);
        $propertyAvailableRoomRate->setRoomTypesAvailable(RoomTypeAvailableFactory::makeCollection($entity->room_types_available));

        return $propertyAvailableRoomRate;
    }

    /**
     * @param $entities
     * @return PropertyAvailableRoomRate[]
     */
    public static function makeCollection($entities)
    {
        $propertyAvailableRoomRates = collect();
        foreach ($entities as $entity) {
            $propertyAvailableRoomRates->push(self::make($entity));
        }

        return $propertyAvailableRoomRates;
    }
}