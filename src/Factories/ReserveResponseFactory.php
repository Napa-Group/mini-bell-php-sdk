<?php

namespace MiniBell\Factories;

use MiniBell\Entities\ReserveResponse;

class ReserveResponseFactory implements IFactory
{
    /**
     * @param $entity
     * @return ReserveResponse
     * @throws \Exception
     */
    public static function make($entity)
    {
        $reserveResponse = new ReserveResponse();

        $reserveResponse->setErrors($entity->errors ? ErrorFactory::makeCollection($entity->errors) : []);
        $reserveResponse->setReserveDetails(ReserveDetailsFactory::make($entity->value->reserve));

        return $reserveResponse;
    }

    /**
     * @param $entities
     * @return ReserveResponse[]
     * @throws \Exception
     */
    public static function makeCollection($entities)
    {
        $reservesResponse = [];
        foreach ($entities as $entity) {
            $reservesResponse[] = self::make($entity);
        }

        return $reservesResponse;
    }

}