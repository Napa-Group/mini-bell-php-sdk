<?php


namespace MiniBell\Factories;


use MiniBell\Entities\ReceiptResponse;

class ReserveServiceDataResponseFactory implements IFactory
{
    public static function make($entity)
    {
        $receiptResponse = new ReceiptResponse();

        $receiptResponse->setType($entity->type);
        $receiptResponse->setData(null);
        $receiptResponse->setDescription($entity->description);
        $receiptResponse->setPrice($entity->price);
        $receiptResponse->setStatus($entity->status);
        $receiptResponse->setCreatedAt($entity->created_at);
        $receiptResponse->setUpdatedAt($entity->updated_at);

        return $receiptResponse;
    }

    public static function makeCollection($entities)
    {
        $receiptResponseCollection = [];
        foreach ($entities as $entity) {
            $receiptResponseCollection[] = self::make($entity);
        }

        return $receiptResponseCollection;
    }
}