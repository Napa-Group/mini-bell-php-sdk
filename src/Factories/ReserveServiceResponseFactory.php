<?php


namespace MiniBell\Factories;


use MiniBell\Entities\ReserveServiceResponse;

class ReserveServiceResponseFactory implements IFactory
{
    public static function make($entity)
    {
        $reserveServiceResponse = new ReserveServiceResponse();

        $reserveServiceResponse->setType($entity->type);
        $reserveServiceResponse->setData(null);
        $reserveServiceResponse->setDescription($entity->description);
        $reserveServiceResponse->setPrice($entity->price);
        $reserveServiceResponse->setStatus($entity->status);
        $reserveServiceResponse->setCreatedAt($entity->created_at);
        $reserveServiceResponse->setUpdatedAt($entity->updated_at);

        return $reserveServiceResponse;
    }

    public static function makeCollection($entities)
    {
        $reserveServiceResponseCollection = [];
        foreach ($entities as $entity) {
            $reserveServiceResponseCollection[] = self::make($entity);
        }

        return $reserveServiceResponseCollection;
    }
}