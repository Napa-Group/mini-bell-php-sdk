<?php

namespace MiniBell\Factories;


use Exception;
use MiniBell\Entities\PropertyAvailable;

class PropertyAvailableFactory implements IFactory
{
    /**
     * @param $entity
     * @return PropertyAvailable
     * @throws Exception
     */
    public static function make( $entity ){
        $propertyAvailable = new PropertyAvailable();

        $propertyAvailable->setPropertyId($entity->property_id);
        $propertyAvailable->setRoomTypeAvailable(RoomTypeAvailableFactory::makeCollection($entity->room_types_available));

        return $propertyAvailable;
    }

    /**
     * @param $entities
     * @return PropertyAvailable[]
     * @throws Exception
     */
    public static function makeCollection($entities)
    {
        $propertiesAvailable = [];
        foreach ( $entities as $entity ){
            $propertiesAvailable[] = self::make($entity);
        }

        return $propertiesAvailable;
    }

}