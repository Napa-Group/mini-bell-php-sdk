<?php


namespace MiniBell\Factories;


use MiniBell\Entities\ReceiptResponse;

class ReceiptResponseFactory implements IFactory
{
    public static function make($entity)
    {
        $receiptResponse = new ReceiptResponse();

        $receiptResponse->setId($entity->id);
        $receiptResponse->setConfirmationCode($entity->confirmation_code);
        $receiptResponse->setAmount($entity->amount);
        $receiptResponse->setPaymentChannelId($entity->payment_channel_id);
        $receiptResponse->setData($entity->data ? (new DataFactory())->make($entity->data) : null);
        $receiptResponse->setPropertyId($entity->property_id);
        $receiptResponse->setPaymentMethod($entity->payment_method);
        $receiptResponse->setCreatedAt($entity->created_at);
        $receiptResponse->setStatus($entity->status);
        $receiptResponse->setReferenceNumber($entity->reference_number);
        $receiptResponse->setAgencyContractId($entity->agency_contract_id);

        return $receiptResponse;
    }

    public static function makeCollection($entities)
    {
        $receiptResponseCollection = [];
        foreach ($entities as $entity) {
            $receiptResponseCollection[] = self::make($entity);
        }

        return $receiptResponseCollection;
    }
}