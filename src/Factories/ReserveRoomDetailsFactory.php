<?php

namespace MiniBell\Factories;

use Exception;
use MiniBell\Entities\ReserveRoomDetails;

class ReserveRoomDetailsFactory implements IFactory
{
    /**
     * @param $entity
     * @return ReserveRoomDetails
     * @throws Exception
     */
    public static function make($entity)
    {
        $reserveRoomDetails = new ReserveRoomDetails();

        $reserveRoomDetails->setRoomTypeId($entity->room_type_id);
        $reserveRoomDetails->setRatePlanId($entity->rate_plan_id);
        $reserveRoomDetails->setCount($entity->count);
        $reserveRoomDetails->setAdultCount($entity->adult_count);
        $reserveRoomDetails->setGuestFirstName($entity->guest_first_name);
        $reserveRoomDetails->setGuestLastName($entity->guest_last_name);
        $reserveRoomDetails->setGuestPhone($entity->guest_phone);
        $reserveRoomDetails->setGuestEmail($entity->guest_email);
        $reserveRoomDetails->setGuestNationalCode($entity->guest_national_code);
        $reserveRoomDetails->setGuestPassportNumber($entity->guest_passport_number);
        $reserveRoomDetails->setGuestCountryId($entity->guest_country_id);
        $reserveRoomDetails->setGuestCityId($entity->guest_city_id);
        $reserveRoomDetails->setChildrenAges($entity->children_ages);
        $reserveRoomDetails->setTotalRackPrice($entity->total_rack_price);
        $reserveRoomDetails->setTotalAgencyPrice($entity->total_agency_price);
        $reserveRoomDetails->setTotalGuestPrice($entity->total_guest_price);
        $reserveRoomDetails->setGuests($entity->guests ? ReserveRoomGuestFactory::makeCollection($entity->guests) : []);
        $reserveRoomDetails->setPrices(RoomRateFactory::makeCollection($entity->prices));
        $reserveRoomDetails->setRoomType(RoomTypeDetailsFactory::make($entity->room_type));
        $reserveRoomDetails->setRatePlan(RatePlanDetailsFactory::make($entity->rate_plan));

        return $reserveRoomDetails;
    }

    /**
     * @param $entities
     * @return ReserveRoomDetails[] $ReserveRoomDetails
     * @throws Exception
     */
    public static function makeCollection($entities)
    {
        $ReserveRoomsDetails = [];
        foreach ($entities as $entity) {
            $ReserveRoomsDetails[] = self::make($entity);
        }

        return $ReserveRoomsDetails;
    }
}