<?php

namespace MiniBell\Factories;

use MiniBell\Entities\RatePlanAvailable;

class RatePlanAvailableFactory implements IFactory
{
    /**
     * @param $entity
     * @return RatePlanAvailable
     * @throws \Exception
     */
    public static function make( $entity ){
        $ratePlanAvailable = new RatePlanAvailable();

        $ratePlanAvailable->setRatePlanId($entity->rate_plan_id);
        $ratePlanAvailable->setRoomRates(RoomRateFactory::makeCollection($entity->room_rates));

        return $ratePlanAvailable;
    }

    /**
     * @param $entities
     * @return RatePlanAvailable[] $ratePlansAvailable
     * @throws \Exception
     */
    public static function makeCollection($entities)
    {
        $ratePlansAvailable = [];
        foreach ( $entities as $entity ){
            $ratePlansAvailable[] = self::make($entity);
        }

        return $ratePlansAvailable;
    }

}