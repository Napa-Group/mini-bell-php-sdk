<?php

namespace MiniBell\Factories;

interface IFactory
{
    /**
     * @param $entity
     */
    public static function make($entity);

    /**
     * @param $entities
     */
    public static function makeCollection($entities);
}