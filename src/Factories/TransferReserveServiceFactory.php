<?php

namespace MiniBell\Factories;

use MiniBell\Entities\TransferReserveService;

class TransferReserveServiceFactory implements IFactory
{
    public static function make($entity)
    {
        $receipt = new TransferReserveService();

        $receipt->setVehicle($entity->vehicle);
        $receipt->setVehicleNumber($entity->vehicle_number);
        $receipt->setDepartureTime($entity->departure_time);
        $receipt->setArrivalTime($entity->arrival_time);
        $receipt->setPassengerPhone($entity->passenger_phone);
        $receipt->setDescription($entity->description);

        return $receipt;
    }

    public static function makeCollection($entities)
    {
        $receiptCollection = [];
        
        foreach ($entities as $entity) {
            $receiptCollection[] = self::make($entity);
        }

        return $receiptCollection;
    }
}