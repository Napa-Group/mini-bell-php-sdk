<?php

namespace MiniBell\Factories;

use MiniBell\Entities\Property;

class PropertyFactory implements IFactory
{
    /**
     * @param $entity
     * @return Property
     */
    public static function make($entity)
    {
        $property = new Property();

        $property->setId($entity->id);
        $property->setName($entity->name);
        $property->setCityId($entity->city_id);
        $property->setCityName($entity->city_name);
        $property->setProvinceId($entity->province_id);
        $property->setProvinceName($entity->province_name);
        $property->setStar($entity->star);
        $property->setType($entity->type);
        $property->setGrade($entity->grade);
//        $property->setLatitude( $entity->latitude );
//        $property->setLongitude( $entity->longitude );
        $property->setRoomsCount($entity->rooms_count);
        $property->setAddress($entity->address);
        $property->setDescription($entity->description);
        $property->setDisabled($entity->disabled);

        return $property;
    }

    /**
     * @param $entities
     * @return Property[]
     */
    public static function makeCollection($entities)
    {
        $properties = [];
        foreach ($entities as $entity) {
            $properties[] = self::make($entity);
        }

        return $properties;
    }
}