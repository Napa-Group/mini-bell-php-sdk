<?php

namespace MiniBell\Factories;

use MiniBell\Entities\RoomTypeDetails;

class RoomTypeDetailsFactory implements IFactory
{
    /**
     * @param $entity
     * @return RoomTypeDetails
     */
    public static function make($entity)
    {
        $roomType = new RoomTypeDetails();

        $roomType->setId($entity->id);
        $roomType->setName($entity->name);
        $roomType->setType($entity->type);
        $roomType->setCapacity($entity->capacity);
        $roomType->setExtraCapacity($entity->extra_capacity);
        $roomType->setSingleBedCount($entity->single_bed_count);
        $roomType->setDoubleBedCount($entity->double_bed_count);
        $roomType->setSofaBedCount($entity->sofa_bed_count);
        $roomType->setCount($entity->count);
        $roomType->setOutOfService($entity->out_of_service);
        $roomType->setDescription($entity->description);
        $roomType->setRatePlans(isset($entity->rate_plans) ? RatePlanDetailsFactory::makeCollection($entity->rate_plans) : null);
        $roomType->setFacilities(FacilityFactory::makeCollection($entity->facilities));

        return $roomType;
    }

    /**
     * @param $entities
     * @return RoomTypeDetails[]
     */
    public static function makeCollection($entities)
    {
        $roomTypes = [];
        foreach ($entities as $entity) {
            $roomTypes[] = self::make($entity);
        }

        return $roomTypes;
    }
}