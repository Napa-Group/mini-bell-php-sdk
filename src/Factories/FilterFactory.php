<?php


namespace MiniBell\Factories;


use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use MiniBell\Entities\Filter;

class FilterFactory
{

    /**
     *
     * @param array $entity
     * @return Filter
     */
    public function makeWithArray($entity)
    {
        $filter = new Filter();

        $filter->setColumnName($entity['column_name']);
        $filter->setOperand($entity['operand']);
        $filter->setValue($entity['value'] ?? null);

        return $filter;
    }

    /**
     * @param Request $request
     * @param array $allows
     * @return Filter[]|Collection
     */
    public function makeCollectionFromRequest($request, $allows = [])
    {
        $entities = $request->get('filters', []);
        $operands = $request->get('operand', []);

        return $this->makeCollectionFromFiltersAndOperands($entities, $operands, $allows);
    }

    /**
     * @param array $entities
     * @param array $operands
     * @param array $allows
     * @return Filter[]|Collection
     */
    public function makeCollectionFromFiltersAndOperands($entities, $operands, $allows = [])
    {
        $filters = collect();

        foreach ($operands as $columnName => $operand) {
            if( count($allows) > 0 && !in_array($columnName,$allows)){
                continue;
            }

            $filter = new Filter();
            $filter->setColumnName($columnName);
            $filter->setOperand($operand);

            if(isset($entities[$columnName])){
                $filter->setValue($entities[$columnName]);
            }

            $filters->push($filter);
        }

        return $filters;
    }
    /**
     * @param Request $request
     * @param array $allows
     * @return Filter[]|Collection
     */
    public function newMakeCollectionFromRequest($request, $allows = [])
    {
        $entities = $request->get('filters', []);
        $filters = collect();
        foreach ($entities as $entity){
            if( !isset($entity['name']) || ( count($allows) > 0 && !isset($allows[$entity['name']]) ) ){
                continue;
            }
            $entity['column_name'] = $entity['name'];
            $filters->push($this->makeWithArray($entity));
        }
        return $filters;
    }
    /**
     *
     * @param Collection $entities
     */
    public function makeFromCollection(Collection $entities)
    {
        // TODO: Implement makeFromCollection() method.
    }

    /**
     *
     * @param [] $entities
     * @return Filter[]|Collection
     */
    public function makeFromArray($entities)
    {
        $filters = collect();

        foreach ($entities as $entity){
            $filters->push($this->makeWithArray($entity));
        }

        return $filters;
    }

    /**
     *
     * @param \stdClass $entity
     *
     */
    public function make(\stdClass $entity)
    {
        // TODO: Implement make() method.
    }
}