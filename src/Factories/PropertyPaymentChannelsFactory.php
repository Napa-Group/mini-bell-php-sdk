<?php


namespace MiniBell\Factories;


use MiniBell\Entities\PropertyPaymentChannels;

class PropertyPaymentChannelsFactory implements IFactory
{
    /**
     * @param $entity
     * @return PropertyPaymentChannelsFactory
     */
    public static function make($entity)
    {
        $propertyPaymentChannel = new PropertyPaymentChannels();

        $propertyPaymentChannel->setId($entity->id);
        $propertyPaymentChannel->setPropertyId($entity->property_id);
        $propertyPaymentChannel->setName($entity->name);
        $propertyPaymentChannel->setChannel($entity->channel);
        $propertyPaymentChannel->setDescription($entity->description);

        return $propertyPaymentChannel;
    }

    /**
     * @param $entities
     * @return PropertyPaymentChannelsFactory[]
     */
    public static function makeCollection($entities)
    {
        $propertyPaymentChannels = collect();
        foreach ($entities as $entity) {
            $propertyPaymentChannels->push(self::make($entity));
        }

        return $propertyPaymentChannels;
    }
}