<?php

namespace MiniBell\Factories;

use MiniBell\Entities\ReserveService;

class ReserveServiceFactory implements IFactory
{
    public static function make($entity)
    {
        $receipt = new ReserveService();

        $receipt->setType($entity->type);
        $receipt->setData($entity->data);
        $receipt->setDescription($entity->description);

        return $receipt;
    }

    public static function makeCollection($entities)
    {
        $receiptCollection = [];
        
        foreach ($entities as $entity) {
            $receiptCollection[] = self::make($entity);
        }

        return $receiptCollection;
    }
}