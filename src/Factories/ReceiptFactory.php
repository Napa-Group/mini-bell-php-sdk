<?php


namespace MiniBell\Factories;


use MiniBell\Entities\Receipt;

class ReceiptFactory implements IFactory
{
    public static function make($entity)
    {
        $receipt = new Receipt();
        $receipt->setConfirmationCode($entity->confirmation_code);
        $receipt->setAmount($entity->amount);
        $receipt->getPaymentChannelId($entity->payment_channel_id);
        $receipt->getData((new ReceiptDateFactory())->make($entity->data));
        $receipt->getDescription($entity->description);

        return $receipt;
    }

    public static function makeCollection($entities)
    {
        $receiptCollection = [];
        foreach ($entities as $entity) {
            $receiptCollection[] = self::make($entity);
        }

        return $receiptCollection;
    }
}