<?php


namespace MiniBell\Factories;

use MiniBell\Entities\Data;

class DataFactory implements IFactory
{
    public static function make($entity)
    {
        $data = new Data();

        $data->setId($entity->id);
        $data->setReferenceNumber($entity->reference_number);
        $data->setCanceled($entity->canceled);
        $data->setPaid($entity->paid);
        $data->setPaymentBillNumber($entity->payment_bill_number);
        $data->setResult($entity->result);
        $data->setUniqueNumber($entity->unique_number);
        $data->setCancelDate($entity->cancel_date);
        $data->setVerificationNeeded($entity->verification_needed);

        return $data;
    }

    public static function makeCollection($entities)
    {
        $dataCollection = [];
        foreach ($entities as $entity) {
            $dataCollection[] = self::make($entity);
        }

        return $dataCollection;
    }
}