<?php

namespace MiniBell\Factories;

use DateTime;
use Exception;
use MiniBell\Entities\RoomRate;

class RoomRateFactory implements IFactory
{
    /**
     * @param $entity
     * @return RoomRate
     * @throws Exception
     */
    public static function make($entity)
    {
        $roomRate = new RoomRate();

        $roomRate->setDay(new DateTime($entity->day));
        $roomRate->setRackRate($entity->rack_rate);
        $roomRate->setExtendBedRackRate($entity->extended_bed_rack_rate);
        $roomRate->setBabyCotRackRate($entity->baby_cot_rack_rate);
        $roomRate->setAgencyRate($entity->agency_rate);
        $roomRate->setExtendBedAgencyRate($entity->extended_bed_agency_rate);
        $roomRate->setBabyCotAgencyRate($entity->baby_cot_agency_rate);
        $roomRate->setGuestRate($entity->guest_rate);
        $roomRate->setExtendBedGuestRate($entity->extended_bed_guest_rate);
        $roomRate->setBabyCotGuestRate($entity->baby_cot_guest_rate);
        $roomRate->setMinStay($entity->min_stay);
        $roomRate->setMaxStay($entity->max_stay);
        $roomRate->setInventory($entity->inventory);
        $roomRate->setCloseToArrival($entity->close_to_arrival);
        $roomRate->setCloseToDeparture($entity->close_to_departure);
        $roomRate->setClosed($entity->closed);

        return $roomRate;
    }

    /**
     * @param $entities
     * @return RoomRate[] $facilities
     * @throws Exception
     */
    public static function makeCollection($entities)
    {
        $roomRates = [];
        foreach ($entities as $entity) {
            $roomRates[] = self::make($entity);
        }

        return $roomRates;
    }
}