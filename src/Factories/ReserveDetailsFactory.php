<?php

namespace MiniBell\Factories;

use DateTime;
use Exception;
use MiniBell\Entities\ReserveDetails;

class ReserveDetailsFactory implements IFactory
{
    /**
     * @param $entity
     * @return ReserveDetails
     * @throws Exception
     */
    public static function make($entity)
    {
        $reserveDetails = new ReserveDetails();
        $reserveDetails->setPropertyId($entity->property_id);
        $reserveDetails->setCheckIn(new DateTime($entity->check_in));
        $reserveDetails->setCheckOut(new DateTime($entity->check_out));
        $reserveDetails->setConfirmationCode($entity->confirmation_code);
        $reserveDetails->setBookerFirstName($entity->booker_first_name);
        $reserveDetails->setBookerLastName($entity->booker_last_name);
        $reserveDetails->setBookerPhone($entity->booker_phone);
        $reserveDetails->setBookerEmail($entity->booker_email);
        $reserveDetails->setVehicle($entity->vehicle);
        $reserveDetails->setVehicleNumber($entity->vehicle_number);
        $reserveDetails->setState($entity->state);
        $reserveDetails->setStatus($entity->status);
        $reserveDetails->setDiscount($entity->discount);
        $reserveDetails->setTotalRackPrice($entity->total_rack_price);
        $reserveDetails->setTotalAgencyPrice($entity->total_agency_price);
        $reserveDetails->setTotalGuestPrice($entity->total_guest_price);
        $reserveDetails->setDescription($entity->description);
        $reserveDetails->setCreateDate(new DateTime($entity->create_date));
        $reserveDetails->setExpireDate(!is_null($entity->expire_date) ? new DateTime($entity->expire_date) : null);
        $reserveDetails->setCancelDate(!is_null($entity->cancel_date) ? new DateTime($entity->cancel_date) : null);
        $reserveDetails->setRooms(ReserveRoomDetailsFactory::makeCollection($entity->rooms));

        return $reserveDetails;
    }

    /**
     * @param $entities
     * @return ReserveDetails[] $ReserveDetailsCollection
     * @throws Exception
     */
    public static function makeCollection($entities)
    {
        $ReserveDetailsCollection = [];
        foreach ($entities as $entity) {
            $ReserveDetailsCollection[] = self::make($entity);
        }

        return $ReserveDetailsCollection;
    }
}