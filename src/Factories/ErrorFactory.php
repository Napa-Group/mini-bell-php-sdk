<?php

namespace MiniBell\Factories;

use MiniBell\Entities\Error;

class ErrorFactory implements IFactory
{
    /**
     * @param $entity
     * @return ReserveDetails
     */
    public static function make($entity)
    {
        $error = new Error();

        $error->setName($entity->name);
        $error->setMessage($entity->message);

        return $error;
    }

    /**
     * @param $entities
     * @return ReserveDetails[] $ReserveDetailsCollection
     */
    public static function makeCollection($errors)
    {
        $errorsCollection = [];
        foreach ($errors as $error) {
            $errorsCollection[] = self::make($error);
        }

        return $errorsCollection;
    }

    /**
     * @param $entity
     * @return ReserveDetails
     */
    public static function makeFromArray($entity)
    {
        $error = new Error();

        $error->setName($entity['name']);
        $error->setMessage($entity['message']);

        return $error;
    }

    /**
     * @param $entities
     * @return ReserveDetails[] $ReserveDetailsCollection
     */
    public static function makeCollectionFromArray($errors)
    {
        $errorsCollection = [];
        foreach ($errors as $error) {
            $errorsCollection[] = self::makeFromArray($error);
        }

        return $errorsCollection;
    }


}