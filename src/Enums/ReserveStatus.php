<?php

namespace MiniBell\Enums;

class ReserveStatus
{
    const PENDING = 'pending';
    const BOOKING = 'booking';
    const BOOKED = 'booked';
    const REJECTED = 'rejected';
    const OVERBOOKING = 'overbooking';
}