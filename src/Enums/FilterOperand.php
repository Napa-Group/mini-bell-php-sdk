<?php

namespace MiniBell\Enums;

class FilterOperand
{
    const IS_EQUAL_TO = 'IsEqualTo';
    const IS_EQUAL_TO_OR_NULL = 'IsEqualToOrNull';
    const IS_NULL = 'IsNull';
    const IS_NOT_EQUAL_TO = 'IsNotEqualTo';
    const IS_NOT_NULL = 'IsNotNull';
    const START_WITH = 'StartWith';
    const DOES_NOT_CONTAINS = 'DoesNotContains';
    const CONTAINS = 'Contains';
    const ENDS_WITH = 'EndsWith';
    const IN = 'In';
    const NOT_IN = 'NotIn';
    const BETWEEN = 'Between';
    const IS_GREATER_THAN_OR_EQUAL_TO = 'IsGreaterThanOrEqualTo';
    const IS_GREATER_THAN_OR_NULL = 'IsGreaterThanOrNull';
    const IS_GREATER_THAN = 'IsGreaterThan';
    const IS_LESS_THAN_OR_EQUAL_TO = 'IsLessThanOrEqualTo';
    const IS_LESS_THAN = 'IsLessThan';
    const IS_AFTER_THAN_OR_EQUAL_TO = 'IsAfterThanOrEqualTo';
    const IS_AFTER_THAN = 'IsAfterThan';
    const IS_BEFORE_THAN_OR_EQUAL_TO = 'IsBeforeThanOrEqualTo';
    const IS_BEFORE_THAN = 'IsBeforeThan';
}