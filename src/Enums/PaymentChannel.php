<?php


namespace MiniBell\Enums;


class PaymentChannel
{
    const POD = 'pod';
    const MANUAL = 'manual';
}