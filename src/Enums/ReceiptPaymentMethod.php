<?php


namespace MiniBell\Enums;


class ReceiptPaymentMethod
{
    const CARD = 'card';
    const ACCOUNT = 'account';
    const ACH = 'ach';
    const IPG = 'ipg';
    const USSD = 'ussd';
    const CREDIT = 'credit';
    const INVOICE = 'invoice';
}