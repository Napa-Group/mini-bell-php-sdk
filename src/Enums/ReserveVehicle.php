<?php

namespace MiniBell\Enums;

class ReserveVehicle
{
    const BUS = 'bus';
    const TRAIN = 'train';
    const SHIP = 'ship';
    const AIRPLANE = 'airplane';
}