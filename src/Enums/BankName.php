<?php


namespace MiniBell\Enums;


class BankName
{
    const SAMAN = 'saman';
    const MELI = 'meli';
    const SADERAT = 'saderat';
    const MELLAT = 'mellat';
    const SEPAH = 'sepah';
    const PARSIAN = 'parsian';
    const EGHTESAD_NOVIN = 'eghtesad_novin';
    const KESHAVARZI = 'keshavarzi';
    const MASKAN = 'maskan';
    const PASARGAD = 'pasargad';
    const SANAT_VA_MADAN = 'sanat_va_madan';
    const TOSE_SADERAT_IRAN = 'tose_saderat_iran';
    const SARMAYEH = 'sarmayeh';
    const REFAH_KARGARAN = 'refah_kargaran';
    const POST_BANK = 'post_bank';
    const TOSEE_TAAVON = 'tosee_taavon';
    const SINA = 'sina';
    const SHAHR = 'shahr';
    const DAY = 'day';
    const TEJARAT = 'tejarat';
    const ANSAR = 'ansar';
    const KHAVARMIANEH = 'khavarmianeh';
    const GARDESHGARY = 'gardeshgary';
    const HEKMAT_IRANIAN = 'hekmat_iranian';
    const IRAN_ZAMIN = 'iran_zamin';
    const KOUSAR = 'kousar';
    const GAVAMIN = 'gavamin';
    const RESALAT = 'resalat';
    const AYANDEH = 'ayandeh';
    const MEHRIRAN = 'mehr_iran';
    const KARAFARIN = 'karafarin';
    const MELAL = 'melal';
    const MEHR = 'mehr';
    const MEHR_EGHTESAD ='mehr_eghtesad';
    const  KASPIAN = 'kaspian';
    const  NOOR = 'noor';
}