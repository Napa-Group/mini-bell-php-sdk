<?php
namespace MiniBell;

use MiniBell\Exceptions\BaseRuntimeException;
use MiniBell\Factories\PropertyAvailableFactory;
use MiniBell\Factories\ReserveDetailsFactory;

class Webhook
{
    const AVAILABLE_CHANGED = 'room_rate_changed';
    const RESERVE_CHANGED = 'reserve_changed';

    private $actions = [];
    private $body = null;

    /**
     * Webhook constructor.
     * @param \stdClass $body
     */
    function __construct($body)
    {
        $this->body = $body;
        if( empty( $this->body ) ){
            throw new BaseRuntimeException('request body is empty');
        }

        if( !isset( $this->body->method ) || !isset( $this->body->value ) ){
            throw new BaseRuntimeException('body is not valid');
        }
    }

    public function setAvailableChangedHandler(callable $handler)
    {
        $this->addAction(self::AVAILABLE_CHANGED, $handler);
    }

    public function setReserveChangedHandler(callable $handler)
    {
        $this->addAction(self::RESERVE_CHANGED, $handler);
    }

    public function handle()
    {
        switch ($this->getWebhookMethodName()) {
            case self::AVAILABLE_CHANGED:
                return $this->availableChangedResponse();
                break;
            case self::RESERVE_CHANGED:
                return $this->reserveChangedResponse();
                break;
        }
    }

    private function availableChangedResponse()
    {
        $handler = isset($this->actions[self::AVAILABLE_CHANGED]) ? $this->actions[self::AVAILABLE_CHANGED] : function () {};

        $propertiesAvailable = PropertyAvailableFactory::makeCollection($this->body->value->properties_available);
        return call_user_func($handler, $propertiesAvailable);
    }
    private function reserveChangedResponse()
    {
        $handler = isset($this->actions[self::RESERVE_CHANGED]) ? $this->actions[self::RESERVE_CHANGED] : function () {};

        $reserveDetails = ReserveDetailsFactory::make($this->body->value->reserve);
        return call_user_func($handler, $reserveDetails);
    }
    private function addAction($methodName, callable $handler)
    {
        $this->actions[$methodName] = $handler;
    }

    private function getWebhookMethodName()
    {
        return $this->body->method;
    }
}